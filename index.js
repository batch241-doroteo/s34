const express =  require('express');
const app = express();
const port = 5000;

let users = [{
		"username": "johndoe",
		"password": "johndoe1234"
	}]

app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.get('/home', (request, response) => {
	response.send('Welcome to my homepage!')
})

app.get('/users', (request, response) => {
	response.send(users)
})

app.delete('/delete-user', (request, response) => {
	response.send(`User ${request.body.username} has been deleted.`)
})


app.listen(port, () => console.log(`Server running at port ${port}`))